package com.example.victorinasecond;


public class Question {
    static int questionNumber;
    private String image;
    private String text;
    private Answer[] answers;

    public Question(String text, Answer[] answers) {
        this.text = text;
        this.answers = answers;
        this.image = attachImageName();
    }

    public String getText() {
        return text;
    }

    public Answer[] getAnswers() {
        return answers;
    }

    public String getImage() {
        return image;
    }

    private String attachImageName() {
        questionNumber++;
        return String.format("question_%s", questionNumber);
    }

    public static Question[] makeQuestions() {


        Question[] questions = new Question[10];

        questions[0] = new Question("Как называется группа волков?",
                new Answer[]{
                        new Answer("Стая", true),
                        new Answer("Прайд"),
                        new Answer("Табун"),
                        new Answer("Стадо")});

        questions[1] = new Question("На фото изображен?",
                new Answer[]{
                        new Answer("Кашалот"),
                        new Answer("Синий кит", true),
                        new Answer("Китовая акула"),
                        new Answer("Касатка")});

        questions[2] = new Question("Какой океан самый большой?",
                new Answer[]{
                        new Answer("Атлантический океан"),
                        new Answer("Индийский океан"),
                        new Answer("Тихий океан", true),
                        new Answer("Северный Ледовитый океан")});

        questions[3] = new Question("Самая глубокая впадина в океане?",
                new Answer[]{
                        new Answer("Индийский желоб"),
                        new Answer("Тихоокеанский колодец"),
                        new Answer("Великий атлантический разрыв"),
                        new Answer("Марианская впадина", true)});

        questions[4] = new Question("Какой вид акулы изображен на картинке?",
                new Answer[]{
                        new Answer("Лисья акула"),
                        new Answer("Белая акула"),
                        new Answer("Акула молот", true),
                        new Answer("Тигровая акула")});

        questions[5] = new Question("При каких обстоятельствах предположительно погиб актер Сергей Бодров?",
                new Answer[]{
                        new Answer("Передозировка наркотиками"),
                        new Answer("Сход лавины", true),
                        new Answer("Сердечный приступ"),
                        new Answer("Автокатастрофа")});

        questions[6] = new Question("Кто написал книгу '451° по Фаренгейту'?",
                new Answer[]{
                        new Answer("Рэй Брэдбери", true),
                        new Answer("Агата Кристи"),
                        new Answer("Иэн Бэнкс"),
                        new Answer("Чак Палланик")});

        questions[7] = new Question("В каком из этих фильмов не играл Арнольд Шварцнегер?",
                new Answer[]{
                        new Answer("Кактус Джек"),
                        new Answer("Терминатор"),
                        new Answer("Робокоп", true),
                        new Answer("Последний киногерой")});

        questions[8] = new Question("В честь кого назван язык Python?",
                new Answer[]{
                        new Answer("Сына создателя языка Лари Питона"),
                        new Answer("Револьвер Кольт Питон"),
                        new Answer("Змея Питон"),
                        new Answer("Монти Пайтон", true)});

        questions[9] = new Question("Кто придумал биткоин?",
                new Answer[]{
                        new Answer("Сатоши Накомото", true),
                        new Answer("Юкихиро Мацумото"),
                        new Answer("Синдзи Миками"),
                        new Answer("Хаяо Миодзаки")});


        return questions;
    }
}
