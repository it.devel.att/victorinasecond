package com.example.victorinasecond;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();

    public TextView lifeQuantityText;
    public TextView coinQuantityText;

    public RelativeLayout fiftyFiftyHelpButton;
    public RelativeLayout changeQuestionHelpButton;

    public TextView fiftyFiftyHelpCoinCostText;
    public TextView changeQuestionHelpCoinCostText;

    public ImageView questionImageBlock;
    public TextView questionTextBlock;

    public Button answerOneButton;
    public Button answerTwoButton;
    public Button answerThreeButton;
    public Button answerFourButton;

    public Toast wrongAnswer;
    public Toast notEnoughCoin;

    public int lifeQuantity = 4;
    public int coinQuantity = 200;

    private int fiftyFiftyHelpPriceCoin = 40;
    private int changeQuestionHelpPriceCoin = 30;

    private boolean fiftyFiftyHelpUseOnCurrentQuestion = false;

    public int currentQuestionNumber = 0;
    public int totalQuantityOfQuestions;
    public Question currentQuestion;
    public Question[] questions;
    public Answer[] questionAnswers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fiftyFiftyHelpButton = findViewById(R.id.fiftyFiftyHelpButton);
        changeQuestionHelpButton = findViewById(R.id.changeQuestionHelpButton);

        fiftyFiftyHelpButton.setOnClickListener(this);
        changeQuestionHelpButton.setOnClickListener(this);

        lifeQuantityText = findViewById(R.id.lifeQuantityNumber);
        coinQuantityText = findViewById(R.id.coinQuantityNumber);

        fiftyFiftyHelpCoinCostText = findViewById(R.id.fiftyFiftyHelpCoinCostText);
        changeQuestionHelpCoinCostText = findViewById(R.id.changeQuestionHelpCoinCostText);

        fiftyFiftyHelpCoinCostText.setText(String.format("%s", fiftyFiftyHelpPriceCoin));
        changeQuestionHelpCoinCostText.setText(String.format("%s", changeQuestionHelpPriceCoin));

        questionImageBlock = findViewById(R.id.questionImage);
        questionTextBlock = findViewById(R.id.questionText);

        answerOneButton = findViewById(R.id.answerOneButton);
        answerTwoButton = findViewById(R.id.answerTwoButton);
        answerThreeButton = findViewById(R.id.answerThreeButton);
        answerFourButton = findViewById(R.id.answerFourthButton);

        answerOneButton.setOnClickListener(this);
        answerTwoButton.setOnClickListener(this);
        answerThreeButton.setOnClickListener(this);
        answerFourButton.setOnClickListener(this);

        questions = Question.makeQuestions();
        totalQuantityOfQuestions = questions.length;
        currentQuestion = questions[currentQuestionNumber];
        questionAnswers = currentQuestion.getAnswers();
        setCurrentLifeQuantity();
        setCurrentCoinQuantity();

        setTextCurrentQuestion();
        setTextCurrentQuestionAnswers();
    }

    private void setCurrentLifeQuantity() {
        Log.i(TAG, "Call method setCurrentLifeQuantity");

        lifeQuantityText.setText(String.format("%s", lifeQuantity));
    }

    private void setCurrentCoinQuantity() {
        Log.i(TAG, "Call method setCurrentCoinQuantity");

        coinQuantityText.setText(String.format("%s", coinQuantity));
    }


    private void setTextCurrentQuestion() {
        Log.i(TAG, "Call method setTextCurrentQuestion");

        questionTextBlock.setText(currentQuestion.getText());
        questionImageBlock.setImageResource(getResources().getIdentifier(currentQuestion.getImage(), "drawable", this.getPackageName()));
    }

    private void setTextCurrentQuestionAnswers() {
        Log.i(TAG, "Call method setTextCurrentQuestionAnswers");

        answerOneButton.setText(questionAnswers[0].getText());
        answerOneButton.setVisibility(View.VISIBLE);

        answerTwoButton.setText(questionAnswers[1].getText());
        answerTwoButton.setVisibility(View.VISIBLE);

        answerThreeButton.setText(questionAnswers[2].getText());
        answerThreeButton.setVisibility(View.VISIBLE);

        answerFourButton.setText(questionAnswers[3].getText());
        answerFourButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        Log.i(TAG, "call method onClick");

        switch (v.getId()) {
            case R.id.fiftyFiftyHelpButton:
                checkCoinAndExecuteFiftyFiftyHelp();
                break;
            case R.id.changeQuestionHelpButton:
                checkCoinAndExecuteChangeQuestionHelp();
                break;
            case R.id.answerOneButton:
                checkCurrentAnswer(questionAnswers[0]);
                break;
            case R.id.answerTwoButton:
                checkCurrentAnswer(questionAnswers[1]);
                break;
            case R.id.answerThreeButton:
                checkCurrentAnswer(questionAnswers[2]);
                break;
            case R.id.answerFourthButton:
                checkCurrentAnswer(questionAnswers[3]);
                break;
        }
    }

    private void checkCoinAndExecuteFiftyFiftyHelp() {
        Log.i(TAG, "call method checkCoinAndExecuteFiftyFiftyHelp");

        if (fiftyFiftyHelpUseOnCurrentQuestion) {
            Toast alreadyUseFiftyFifty = Toast.makeText(this, "Вы уже использовали подсказку 50/50 на текущем вопросе", Toast.LENGTH_SHORT);
            alreadyUseFiftyFifty.show();
        } else if (coinQuantity < fiftyFiftyHelpPriceCoin) {
            notEnoughCoin = Toast.makeText(this, "Недостаточно монет", Toast.LENGTH_SHORT);
            notEnoughCoin.show();
        } else {
            fiftyFiftyHelpUseOnCurrentQuestion = true;
            coinQuantity -= fiftyFiftyHelpPriceCoin;
            setCurrentCoinQuantity();

            makeTwoRandomWrongAnswersInvisible();
        }
    }

    private void checkCoinAndExecuteChangeQuestionHelp() {
        Log.i(TAG, "call method checkCoinAndExecuteChangeQuestionHelp");

        if (coinQuantity < changeQuestionHelpPriceCoin) {
            notEnoughCoin = Toast.makeText(this, "Недостаточно монет", Toast.LENGTH_SHORT);
            notEnoughCoin.show();
        } else {
            coinQuantity -= changeQuestionHelpPriceCoin;
            setCurrentCoinQuantity();

            setNextQuestion();
        }
    }

    private void makeTwoRandomWrongAnswersInvisible() {
        Log.i(TAG, "call method makeTwoRandomWrongAnswersInvisible");

        Button[] answerButtons = {answerOneButton, answerTwoButton, answerThreeButton, answerFourButton};
        int indexOfRightQuestion = 0;

        for (int i = 0; i < questionAnswers.length; i++)
            if (questionAnswers[i].isCorrect()) {
                indexOfRightQuestion = i;
                break;
            }


        int countOfInvisibleButtons = 0;
        for (int i = 0; i < answerButtons.length; i++) {
            if (i != indexOfRightQuestion && countOfInvisibleButtons < 2) {
                answerButtons[i].setVisibility(View.INVISIBLE);
                countOfInvisibleButtons++;
            }
        }

    }

    private void checkCurrentAnswer(Answer questionAnswer) {
        Log.i(TAG, "call method checkCurrentAnswer");

        if (questionAnswer.isCorrect()) {
            coinQuantity += 20;
            setCurrentCoinQuantity();

        } else {
            lifeQuantity--;
            setCurrentLifeQuantity();

            String message = String.format("Ответ '%s' неправильный!", questionAnswer.getText());
            wrongAnswer = Toast.makeText(this, message, Toast.LENGTH_SHORT);
            wrongAnswer.show();
        }
        setNextQuestion();
    }

    private void setNextQuestion() {
        Log.i(TAG, "Call method setNextQuestion");
        currentQuestionNumber++;
        fiftyFiftyHelpUseOnCurrentQuestion = false;

        if (currentQuestionNumber < totalQuantityOfQuestions) {
            currentQuestion = questions[currentQuestionNumber];
            questionAnswers = currentQuestion.getAnswers();

            setTextCurrentQuestion();
            setTextCurrentQuestionAnswers();
        } else if (lifeQuantity <= 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            AlertDialog alert = builder
                    .setTitle("У вас больше не осталось жизней")
                    .setMessage("Что делать дальше?")
                    .setPositiveButton("Сыграть ещё", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            recreate();
                        }
                    })
                    .setNegativeButton("Выйти", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .create();
            alert.show();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            AlertDialog alert = builder
                    .setTitle("Вы ответили на все вопросы")
                    .setMessage("Что делать дальше?")
                    .setPositiveButton("Сыграть ещё", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Question.questionNumber = 0;
                            recreate();
                        }
                    })
                    .setNegativeButton("Выйти", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .create();
            alert.show();
        }
    }


}
